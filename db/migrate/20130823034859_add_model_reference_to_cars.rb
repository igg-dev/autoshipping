class AddModelReferenceToCars < ActiveRecord::Migration
  def change
    add_reference :cars, :model, index: true
  end
end
