class AddNameSecnameToUser < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :second_name, :string
    add_column :users, :address, :text
    add_column :users, :balance, :integer
  end
end
