class Model < ActiveRecord::Base
  belongs_to :manufacturer

  has_many :cars

  validates_presence_of :name, :manufacturer_id
  validates_uniqueness_of :name, scope: :manufacturer_id

  attr_protected nil

end
