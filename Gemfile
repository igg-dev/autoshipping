source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'

# Use sqlite3 as the database for Active Record
gem 'redis'
gem 'resque'
gem 'sqlite3'
gem 'pg'
gem 'activeadmin',         github: 'gregbell/active_admin', branch: 'rails4' 
gem 'ransack',             github: 'ernie/ransack'
gem 'inherited_resources', github: 'josevalim/inherited_resources'
gem 'formtastic',          github: 'justinfrench/formtastic'
gem 'therubyracer'
gem 'slim'
gem 'protected_attributes'
gem 'paperclip'
gem 'mechanize'
gem 'kaminari'
# gem "meta_search",         github: 'ernie/meta_search',     branch: '1-0-stable'

# Use SCSS for stylesheets
gem 'sass-rails'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
gem 'debugger', group: [:development, :test]
group :development, :test do
  gem 'jazz_hands'
  gem 'capistrano'
  gem 'rvm-capistrano'
  gem 'capistrano-nginx-unicorn', require: false, github:"kalys/capistrano-nginx-unicorn"
end
group :production do
  gem "capistrano-resque", "~> 0.1.0"
  gem 'unicorn'
end
