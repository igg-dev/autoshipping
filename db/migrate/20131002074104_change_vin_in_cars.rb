class ChangeVinInCars < ActiveRecord::Migration
  def change
    change_column :cars, :vin, :string
  end
end
