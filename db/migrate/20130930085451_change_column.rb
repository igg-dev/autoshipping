class ChangeColumn < ActiveRecord::Migration
  def change
    change_column :cars, :engine_volume, :float
    add_column :cars, :damage, :string
    add_column :cars, :link, :string
  end

end
