jQuery(document).ready(function($) {
    var gallery = $('#bx-pager').galleriffic({
        delay:                     2500,
        numThumbs:                 40,
        preloadAhead:              10,
        enableTopPager:            false,
        enableBottomPager:         false,
        maxPagesToShow:            7,
        imageContainerSel:         '#slide',
        controlsContainerSel:      '',
        captionContainerSel:       '',
        loadingContainerSel:       '',
        renderSSControls:          false,
        renderNavControls:         false,
        playLinkText:              'Play Slideshow',
        pauseLinkText:             'Pause Slideshow',
        prevLinkText:              '&lsaquo; Previous Photo',
        nextLinkText:              'Next Photo &rsaquo;',
        nextPageLinkText:          'Next &rsaquo;',
        prevPageLinkText:          '&lsaquo; Prev',
        enableHistory:             false,
        autoStart:                 false,
        syncTransitions:           false,
        defaultTransitionDuration: 900,
        onSlideChange:             '',
        onPageTransitionOut:       function(callback) {
            this.fadeTo('fast', 0.0, callback);
        },
        onPageTransitionIn:        function() {
            this.fadeTo('fast', 1.0);
        }
    });
});
    