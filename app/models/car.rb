class Car < ActiveRecord::Base
  before_create :rus_translate
  scope :index, -> { where.not(:engine_volume=> 0.0) }
  scope :state, -> (d){where(:state=> d)}
  belongs_to :model
  
  has_one :manufacturer, through: :model
  has_many :images, dependent: :destroy

  attr_protected nil

  validates_presence_of :model, :release_year, :manufacturer_id

  BODY_TYPES = %w(sedan minivan coupe convertible hatchback crossover suv pickup fullsize-van truck moto)
  private
    def rus_translate
      self.damage= I18n.t self.damage

      self.transmission= I18n.t self.transmission
    end
end