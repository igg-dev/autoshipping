ActiveAdmin.register Car do
  scope :all, :default => true
  scope :mine do |cars|
    cars.where(:source_site => '')
  end
  filter :car_body, collection: Car::BODY_TYPES, as: :check_boxes
  filter :manufacturer, :as => :select, :collection => proc { Manufacturer.all }
  index do
    selectable_column
    column :id
    column :manufacturer
    column :model
    column :release_year
    column :drive
    column :engine_volume
    column :odometer
    column :source_site
    default_actions
  end
end
