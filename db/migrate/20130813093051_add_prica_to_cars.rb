class AddPricaToCars < ActiveRecord::Migration
  def change
    add_column :cars, :price, :integer
    add_column :cars, :transmission, :string
    add_column :cars, :car_body, :string
  end
end
