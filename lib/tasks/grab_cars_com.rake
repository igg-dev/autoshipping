# encoding: utf-8
namespace :grab do
  desc "Import data from cars com"
  task :cars_com => :environment do

    require 'rubygems'
    require 'mechanize'
    require 'logger'
    require 'redis'
    require 'json'
    require 'resque/tasks'

    agent = Mechanize.new { |a| a.log = Logger.new(STDERR) }
    redis=Redis.new
    car_body_types=['sedan', 'minivan', 'coupe', 'convertible', 'hatchback', 'crossover', 'suv', 'pickup', 'fullsize-van', 'truck', 'moto']
    site='http://www.cars.com'
    start_url='http://www.cars.com/for-sale/_/N-m5d?sf1Dir=DESC&mdId=21095&rd=100000&zc=64123&PMmt=1-1-0&sf2Dir=ASC&sf1Nm=price&sf2Nm=location&rpp=250&feedSegId=28705&searchSource=GN_BREADCRUMB&crSrtFlds=feedSegId-mdId&pgId=2102'
      # car body types: sedan, minivan, coupe, convertible, hatchback, crossover, suv, pickup, fullsze_van, truck, moto
    page=agent.get start_url
    models=page.search('a').find_all{|l| l.attr('name')=~/Model/}
    models.each do |model|
      begin
        page=agent.get model.attr('href')
      rescue  Net::HTTPInternalServerError
        next
      end
      if number_cars(page)
        prices=page.search('a').find_all{|l| l.attr('name')=~/Price/}
        prices.each do |price|
          begin
            page=agent.get price.attr('href')
          rescue  Net::HTTPInternalServerError
            next
          end 
          if number_cars(page)
            colors=page.search('a').find_all{|l| l.attr('name')=~/Externnal Colors/}
            colors.each do |color|
              page=agent.get color.attr('href')
              go_thru_pages page, redis
            end
          else
            go_thru_pages page, redis
          end
        end 
      else
        go_thru_pages page, redis
      end
    end  
  end

  SITE='http://www.cars.com'
  def number_cars page
    text=page.search('div[class="col40"]')[0].text
    return text=page.search('div[class="col40"]')[0].text.to_i>5000
  end
  def go_thru_pages page, redis_obj
    # begin
      # page=agent.click(page.links_with(:class=>/right/)[0]) if page.links_with(:class=>/left disable/).empty? 
      divs=page.search('.vehicle').first(10) #SELECT FIRST 10
      grab_push_data divs, redis_obj
    # end while not page.links_with(:class=>/right disable/)[0]   
  end
  def grab_push_data divs, redis
    divs.each_with_index do |dv,index|
        return if index>10
        link=SITE+dv.search('a')[2].attr('href')
        release_year=dv.search('a')[2].search('span')[1].text.to_i
        car_make=dv.search('a')[2].search('span')[2].text.split(' ')[0]
        car_model=dv.search('a')[2].search('span')[2].text.gsub(car_make,'').strip.split.map(&:capitalize)*'_'
        redis_json={release_year=>link}.to_json
        redis.rpush("cars_com:#{car_model}", redis_json)
        Resque.enqueue(CarGrabber,"cars_com",car_make,car_model)
    end 
  end
end