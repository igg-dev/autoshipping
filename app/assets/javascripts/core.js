$(document).ready(function () { 
	$("select, input, textarea").uniform();
	$("input").not(".button").uniform();

  // $("#sidebar").css('min-height', ($(document).height() + 250) + 'px');

  $('.scroller').baron();

  $('.hint').click(function(){ 
    $(this).hide();
  });

  $('.blc_faqlist .item').click(function(){
  	$('.blc_faqlist .item').find('.node').hide(200);
  	$('.blc_faqlist .item').removeClass('opened');
  	$(this).find('.node').toggle(200);
  	$(this).addClass('opened');
  });

  $("#content").css('min-height', $(document).height() + 'px');

  $('#myTab a').click(function (e) {
    e.preventDefault()
    $(this).tab('show')
  })


});