class AddDriveToCars < ActiveRecord::Migration
  def change
    add_column :cars, :drive, :string
    add_column :cars, :engine_volume, :integer
    add_column :cars, :odometer, :integer
    add_column :cars, :color, :string
    add_column :cars, :vin, :integer
  end
end
