class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.references :car, index: true

      t.timestamps
    end
  end
end
