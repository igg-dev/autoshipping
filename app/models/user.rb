class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  attr_accessible :name, :last_name, :email, :address, :password, :password_confirmation, :remember_me
  attr_accessible :avatar
  validates :name, :last_name, :email, :address, presence: {message: "Поле должно быть заполнено"}
  has_attached_file :avatar, :styles => { :medium => "120x120>", :thumb => "60x60>" }
end 