class AddIndexToModels < ActiveRecord::Migration
  def change
    add_index :models, :manufacturer_id
  end
end
