# encoding: utf-8
require 'rubygems'
require 'mechanize'
require 'logger'
require 'redis'
namespace :fetch do 
  task :cars_com, [:make, :model] =>[:environment] do |t,args|
    agent = Mechanize.new { |a| a.log = Logger.new(STDERR) }
    redis=Redis.new
    agent.pluggable_parser.default = Mechanize::DirectorySaver.save_to  '../../app/assets/images/cars_com'
    dir_name='../../app/assets/images/cars_com'
    photo_urls='http://www.cars.com/go/search/'
    car_make=args[:make]
    car_model=args[:model]
    redis_json=redis.lpop("cars_com:#{car_model}")
    release_year=JSON.parse(redis_json).keys.first.to_i
    car_link=JSON.parse(redis_json).values.first
    car_model=car_model.split('_')*' '
    page=agent.get car_link
    man=Manufacturer.where(:title=>car_make.capitalize).first_or_create
    model=Model.where(:name=>car_model.split.map(&:capitalize)*' ', :manufacturer_id=> man.id).first_or_create
    data=page.search('#vehicleListBox')
    price=page.search('.vehiclePrice').text.strip.gsub('$','').gsub(',','').to_i
    transmission=data.search('#Transmission').text.try(:gsub,"Transmission:",'').try(:strip)||''
    transmission=transmission.scan(/(auto)|(manual)|(cvt)/i).first.select{|t| !t.nil?}.first
    car_body=data.search('#BodyStyle').text.gsub('Body Style:','').strip.downcase
    state=car_link.include?('tracktype=used')? 'used' : 'new'
    drive=data.search('#Drivetrain').text.try(:gsub,'Drivetrain:','').to_s.strip||''
    engine_volume=data.search('#Engine').text.strip.split(' ')[1].try(:gsub,'L','').to_f||0
    odometer=data.search('#Mileage').text.try(:gsub, "Mileage:",'').try(:gsub,',','').to_i||0
    color=data.search('#ExteriorColor').text.try(:gsub,"Exterior Color:",'').to_s.strip.downcase||''
    vin=data.search('#VIN').text.try(:gsub,'VIN:','').to_s.strip||''
    damage=' '
    car=Car.where(:model_id=>model.id, :release_year=>release_year, :transmission=>transmission, :car_body=>car_body, :price=>price, :manufacturer_id=>man.id, :state=> state, :drive=>drive, :engine_volume=>engine_volume, :odometer=>odometer,:source_site=>'cars_com', :color=>color, :vin=>vin, :damage=>damage, :link=>car_link).first_or_create
    photo_page=agent.get photo_urls+page.links_with(:id=>%r{photosTabLink}).first.href
    photo_page.images_with(:class=>%r{photo}).each do |img|
      source=img.src.gsub('preview','supersized')
      image=Image.where(:url=>source).first_or_create
      image.from_url=(source)
      image.car=car
      image.save!
     end
    car.save!
  end  
end