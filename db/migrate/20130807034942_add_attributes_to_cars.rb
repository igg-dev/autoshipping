class AddAttributesToCars < ActiveRecord::Migration
  def change
    add_column :cars, :model, :string
    add_column :cars, :release_year, :integer
  end
end
