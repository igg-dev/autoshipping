namespace :fetch do
  task :iaai =>:environment do

      require 'rubygems'   
      require 'mechanize'
      require 'logger'

    agent = Mechanize.new { |a| a.log = Logger.new(STDERR) }

    site='https://www.iaai.com/Vehicles/Search.aspx?RefinerSetName=Vehicle+Type&RefinerName=Automobiles'
    dir_name='/home/ig/autoshipping/app/assets/images/iaai_com/'
    page=agent.get site

    for i in 0..49 do
      begin
        agent.transact do
          car_page=agent.click(page.links_with(:href=>/VehicleDetails/)[i])
          transmission=car_page.search('#ctl00_ContentPlaceHolder1_DivEngineDriveTrainPlaceHolder').search('li')[3].text
          odometer=car_page.search('#ctl00_ContentPlaceHolder1_DivStaticConditionsPlaceHolder').search('li')[5].text
          color=car_page.search('#ctl00_ContentPlaceHolder1_DivVINInfoPlaceHolder').search('li')[7].text
          drive=car_page.search('#ctl00_ContentPlaceHolder1_DivEngineDriveTrainPlaceHolder').search('li')[4].text
          car_body=car_page.search('#ctl00_ContentPlaceHolder1_DivVINInfoPlaceHolder').search('li')[3].text
          engine_volume=car_page.search('#ctl00_ContentPlaceHolder1_DivEngineDriveTrainPlaceHolder').search('li')[2].text
        end
      rescue Timeout::Error
        sleep 120
        retry
      rescue Exception=>e
        case e.message
        when /404/ then next
        end
      end
      page.search('.searchData').search('tr.auction')[i]

    end

  end  
end