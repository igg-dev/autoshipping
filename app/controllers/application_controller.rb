class ApplicationController < ActionController::Base
  before_action :configure_devise_params, if: :devise_controller?

  protect_from_forgery with: :exception
  protected
  def configure_devise_params
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:name, :last_name, :email, :address, :password, :password_confirmation)
    end
    devise_parameter_sanitizer.for(:account_update) do |u|
      u.permit(:name, :last_name, :email, :address,  :password, :password_confirmation, :current_password)
    end
  end
end
