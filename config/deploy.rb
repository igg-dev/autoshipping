require 'bundler/capistrano'
require "capistrano-resque"
load 'deploy/assets'
set :application, "sds"
set :user, 'deploy'
set :scm, :git
set :repository,  "git@131.103.20.167:mimikadze/sds-global-shipping.git"
set :branch, 'master'
set :use_sudo, false
set :deploy_to, "/home/deploy/apps/#{application}"
set :deploy_via, :remote_cache
role :app, "10.0.0.43"
role :web, "10.0.0.43"
role :db,  "10.0.0.43", :primary => true
set :nginx_server_name, "localhost sds.com www.sds.com sds.spalmalo.com www.sds.spalmalo.com sds"
set :server_name, "10.0.0.43"
set :unicorn_workers, 3
set :rvm_ruby_string, "ruby-2.0.0-p247@#{application}"
role :resque_worker, "10.0.0.43"
role :resque_scheduler, "10.0.0.43"
set :workers, { "cars_to_grab" => 1 }
before "deploy:setup", "rvm:create_gemset"
after 'deploy', 'deploy:cleanup'
after "deploy:restart", "resque:restart"
require 'rvm/capistrano'
require 'capistrano-nginx-unicorn'