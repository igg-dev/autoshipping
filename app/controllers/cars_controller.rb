class CarsController < ApplicationController
  before_action :show_default_new_cars, only: :auction
  before_action :ensure_search_params
  def index
     @q = Car.index.search(params[:q])
     @cars=@q.result(distinct: true).page(params[:page]).per(12)
  end   

  def show
    @car = Car.find params[:id]   
    @related=Car.where("id !=? AND model_id =?", @car.id, @car.model_id).limit(3)
  end

  def catalog
    @q = Car.search(params[:q])
    @cars=@q.result(distinct: true).where(:source_site=>'').page(params[:page]).per(12)
  end

  def auction
    @q = Car.search(params[:q])
    @cars=@q.result(distinct: true).where.not(source_site:'none').state(params[:state]).page(params[:page]).per(16)
  end

  def search
    @q = Car.index.search(params[:q])
    @cars=@q.result(distinct: true).page(params[:page]).per(18)
  end

  private
    def show_default_new_cars
      params[:state]||= 'new'
    end
    def ensure_search_params
      params[:q] ||= {}
    end
end
