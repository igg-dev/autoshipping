class ChangeColumnLinkInCars < ActiveRecord::Migration
  def change
    change_column :cars, :link, :text
  end
end
