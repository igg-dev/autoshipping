class Manufacturer < ActiveRecord::Base

  has_many :models
  has_many :cars, through: :models
  attr_protected nil

  validates_presence_of :title
end
