class AddStateToCars < ActiveRecord::Migration
  def change
    add_column :cars, :state, :string
  end
end
