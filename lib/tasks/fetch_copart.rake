require 'rubygems'
require 'mechanize'
require 'logger'
require 'redis'
namespace :fetch do
  task :copart, [:make, :model] => [:environment] do |t,args|
    agent = Mechanize.new { |a| a.log = Logger.new(STDERR) }
    redis=Redis.new
    site='http://www.copart.com/'
    agent.pluggable_parser.default = Mechanize::DirectorySaver.save_to  './app/assets/images/copart_com'
    car_make=args[:make]
    car_model=args[:model]
    redis_json=redis.lpop("copart:#{car_model}")
    release_year=JSON.parse(redis_json).keys.first.to_i
    car_link=JSON.parse(redis_json).values.first
    man=Manufacturer.where("title LIKE ?", "%#{car_make}%").first_or_create      
    model= Model.where("name LIKE ? AND manufacturer_id= ?", "%#{car_model}%", man.id).first_or_create
    car_page=agent.get car_link
    price=car_page.search('.listTable.w800').search('td')[6].text.try(:split).try(:first).try(:strip).try(:gsub,'$','').try(:gsub, /usd/i,'').try(:gsub, ',','').try(:strip).to_i||0
    odometer=car_page.search('.listTable.w800').search('td')[13].text.try(:split).try(:first).try(:strip).try(:gsub,',','').to_i||''
    damage=car_page.search('.listTable.w800').search('td')[15].text.try(:strip).to_s.downcase||''
    vin=car_page.search('.listTable.w800').search('td')[19].text.try(:strip)||''
    car_body=car_page.search('.listTable.w800').search('td')[21].text.try(:strip)||''
    color=car_page.search('.listTable.w800').search('td')[23].text.try(:strip).try(:downcase)||''
    engine_volume=car_page.search('.listTable.w800').search('td')[25].text.try(:split).try(:first).try(:strip).to_f
    transmission=' '
    state='damaged'
    drive=car_page.search('.listTable.w800').search('td')[27].text.try(:strip)||''
    drive=drive.scan(/(all)|(rear)|(front)|(4)/i).first.select{|t| !t.nil?}.first.chr.capitalize+'WD'
    car=Car.where(:model_id=>model.id, :release_year=>release_year, :transmission=>transmission, :car_body=>car_body, :price=>price, :manufacturer_id=>man.id, :state=> state, :drive=>drive, :engine_volume=>engine_volume, :odometer=>odometer,:source_site=>'copart', :color=>color, :vin=>vin, :damage=>damage, :link=>car_link).first_or_create
    car_page.images_with(:id=>/LotImage/).each do |im|
      source='HTTP://IMAGES.COPART.'<<(im.src.split('.')[-2]<<"X")<<".JPG"
      begin
        image=Image.where(:url=>source).first_or_create
        image.from_url=(source)
        image.car=car
        image.save
      rescue Timeout::Error
        sleep 10
        retry
      rescue 
        next
      end
    end 
# CREATING CAR INSTANCE

    car.save
  end
end