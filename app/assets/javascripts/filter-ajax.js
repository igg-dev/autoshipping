$(document).ready(function () { 
  function contentHeight() {
    var content_height = $('#content').height();
    var sidebar_height = $('#sidebar').height();
    if (content_height < sidebar_height) {
      $('#content').css('height', sidebar_height + 'px');
    }
  }
  contentHeight();
  models = $('#q_model_id_eq').html()
  car_body=$('#q_car_body_eq').val()
  // SOME EXPLICIT MODIFICATIONS
  $('#q_manufacturer_id_eq').attr("id","markSelect").prepend('<option>Все</option>').addBack().prev().css('width', '146px').parent().attr("id","uniform-markSelect").css('width', '160px');
  $('#q_model_id_eq').attr("id","modelSelect").prev().css('width', '146px').parent().attr("id","uniform-modelSelect").css('width', '160px');
  $('#markSelect').change(function(){
    make = $('#markSelect :selected').text();
      $('#modelSelect:first').attr("selected", "selected");
      options='';      
    if (make!='Все' && make!='Выберите марку'){
        options = $(models).filter("optgroup[label="+make+"]").html();
      }
    if (options){
      var i=$(options).first().val();
      var firstM=$("#modelSelect").find("option[value='"+i+"']");
      $(firstM).prop('selected', true);
       $("#modelSelect").prev('span').html(firstM.text());
    }
    else{
      $('#modelSelect').html(models)
    } 
    });

  $('#modelSelect').change(function(){
    var make=$('#modelSelect :selected').parent().attr('label');
    var make = (typeof make === 'undefined') ? 'Все' : make;
    $("#markSelect option").filter(function() {
    return $(this).text() == make; 
    }).prop('selected', true).parent('span').html(make);
    $("#markSelect").prev('span').html(make);
  });
  var year_gt= $("#q_release_year_gt").val()|| 1970
  var year_lt= $("#q_release_year_lt").val()|| 2000
  $( ".filterbar" ).slider({
      range: true,
      min: 1940,
      max: 2013,
      values: [ year_gt, year_lt ],
      slide: function( event, ui ) {
        $( ".filterbar #q_release_year_gt" ).val( ui.values[ 0 ]);
        $( ".filterbar #q_release_year_lt" ).val( ui.values[ 1 ]); }
  }).find(".ui-slider-handle").eq(0).append($( ".filterbar #q_release_year_gt" )).end().eq(1).append($( ".filterbar #q_release_year_lt" ));
  $( ".filterbar #q_release_year_gt" ).val( $(  ".filterbar" ).slider( "values", 0 ));
  $( ".filterbar #q_release_year_lt" ).val( $(  ".filterbar" ).slider( "values", 1 ));
  $('.cars_body a').click(function(e){
    e.preventDefault();
    $('#q_car_body_eq').val($(this).attr("class").split("-")[1]);
  });
  $('.megafilter .leftmenu ul li a').click(function(e){
    e.preventDefault();
    $('#q_car_body_eq').val($(this).attr("href").split("=")[1].split("&")[0]);
    $('#new_q').submit();
  });
});