class Image < ActiveRecord::Base
  attr_reader :from_url
  belongs_to :car

  has_attached_file :file,
    styles: {small: ["70x70>", :jpg], big: ["417x299>", :jpg]}
  def from_url=(url_value)
    self.file = URI.parse(url_value)
    @from_url=url_value
  end
  def image_url
    self.file.url(:small)
  end
end
